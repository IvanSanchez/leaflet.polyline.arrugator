/**
 * What follows is Volodymir Agafonkin's tinyqueue.
 *
 * See https://github.com/mourner/tinyqueue
 *
 * ---
 *
 * ISC License
 *
 * Copyright (c) 2017, Vladimir Agafonkin
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright notice
 * and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */
class TinyQueue {
	constructor(data = [], compare = defaultCompare) {
		this.data = data;
		this.length = this.data.length;
		this.compare = compare;

		if (this.length > 0) {
			for (let i = (this.length >> 1) - 1; i >= 0; i--) this._down(i);
		}
	}

	push(item) {
		this.data.push(item);
		this.length++;
		this._up(this.length - 1);
	}

	pop() {
		if (this.length === 0) return undefined;

		const top = this.data[0];
		const bottom = this.data.pop();
		this.length--;

		if (this.length > 0) {
			this.data[0] = bottom;
			this._down(0);
		}

		return top;
	}

	peek() {
		return this.data[0];
	}

	_up(pos) {
		const { data, compare } = this;
		const item = data[pos];

		while (pos > 0) {
			const parent = (pos - 1) >> 1;
			const current = data[parent];
			if (compare(item, current) >= 0) break;
			data[pos] = current;
			pos = parent;
		}

		data[pos] = item;
	}

	_down(pos) {
		const { data, compare } = this;
		const halfLength = this.length >> 1;
		const item = data[pos];

		while (pos < halfLength) {
			let left = (pos << 1) + 1;
			let best = data[left];
			const right = left + 1;

			if (right < this.length && compare(data[right], best) < 0) {
				left = right;
				best = data[right];
			}
			if (compare(best, item) >= 0) break;

			data[pos] = best;
			pos = left;
		}

		data[pos] = item;
	}
}

function defaultCompare(a, b) {
	return a < b ? -1 : a > b ? 1 : 0;
}

/// What follows is Iván Sánchez's LineArrugator
///
/// See https://gitlab.com/IvanSanchez/arrugator

// A variant of Arrugator that works with segment strings (AKA "lines") instead
// of triangle meshes.
class LineArrugator {
	// #projector;
	// #verts;

	constructor(projector, verts) {
		// The projector function. Must be able to take
		// an array of two numbers [x,y] and return an array of
		// two numbers.
		// The typical use case is a proj4(from,to).forward function.
		this._projector = projector;

		// A two-dimensional array of vertex coordinates. Each vertex is a
		// two-element [x,y] array.
		this._verts = verts;

		// A two-dimensional array of vertex coordinates, projected. Each
		// vertex is a two-element [x,y] array.
		this._projVerts = verts.map(projector);

		// A priority queue of segments, ordered by their epsilons, in descending order.
		this._queue = new TinyQueue([], function (a, b) {
			return b.epsilon - a.epsilon;
		});

		for (let i = 0, l = this._verts.length - 1; i < l; i++) {
			this._calcSegment(i, i + 1);
		}

		// Keeps the indices of the vertices, in connectivity order
		this._order = Array.from({ length: this._verts.length }, (v, i) => i);
	}

	// Calculates data for a segment and pushes it to the priority queue.
	_calcSegment(v1, v2) {
		const midpoint = [
			(this._verts[v1][0] + this._verts[v2][0]) / 2,
			(this._verts[v1][1] + this._verts[v2][1]) / 2,
		];
		const projectedMid = this._projector(midpoint);
		const midProjected = [
			(this._projVerts[v1][0] + this._projVerts[v2][0]) / 2,
			(this._projVerts[v1][1] + this._projVerts[v2][1]) / 2,
		];

		const epsilon =
			(projectedMid[0] - midProjected[0]) ** 2 +
			(projectedMid[1] - midProjected[1]) ** 2;

		this._queue.push({
			v1: v1,
			v2: v2,
			epsilon: epsilon,
			midpoint: midpoint,
			projectedMid: projectedMid,
		});
	}

	step() {
		const top = this._queue.pop();

		const v1 = top.v1;
		const v2 = top.v2;

		const vm = this._verts.length;
		this._verts[vm] = top.midpoint;
		this._projVerts[vm] = top.projectedMid;
		this._order.splice(this._order.indexOf(v2), 0, vm);

		this._calcSegment(v1, vm);
		this._calcSegment(vm, v2);
	}

	// Outputs a copy of the coordinates for the linestring.
	output() {
		return this._order.map((i) => this._projVerts[i]);
	}

	// Subdivides the mesh until the maximum segment epsilon is below the
	// given threshold.
	// The `targetEpsilon` parameter must be in the same units as the
	// internal epsilons: units of the projected CRS, **squared**.
	lowerEpsilon(targetEpsilon) {
		while (this._queue.peek().epsilon > targetEpsilon) {
			this.step();
		}
	}

	get epsilon() {
		return this._queue.peek().epsilon;
	}

	set epsilon(ep) {
		return this.lowerEpsilon(ep);
	}
}

/// Aux method to define CRSs into `proj4js`.

/// Expects `proj4` to be defined as a global!!!!!

function _defCRS(code) {
	const [_, org, number] = /(\w+):(\d+)/.exec(code);

	return fetch(`https://crs-explorer.proj.org/wkt1/${org}/${number}.txt`)
		.catch(() =>
			fetch(`https://spatialreference.org/ref/${org}/${number}/ogcwkt/`)
		)
		.then((res) => res.text())
		.then((wkt) => proj4.defs(code, wkt));
}

/// Polyline.Arrugator itself.

/// Expects `proj` to be defined as a global!!!!!

/// Assumes EPSG:4326 as the Leaflet data CRS for LatLngs!!!!!

/// The input must be an array `[x, y]` in the `srcCRS`, not LatLngs!!!!!!!

L.Polyline.Arrugator = L.Polyline.extend({
	options: {
		srcCRS: "EPSG:4326",
		dstCRS: "EPSG:3857",
		epsilon: 1000000, // Smaller for more detail; bigger for less detail.
	},

	initialize: function initialize(latlngs, options = {}) {
		L.Polyline.prototype.initialize.call(this, latlngs, options);

		Promise.all([
			_defCRS(this.options.srcCRS),
			_defCRS(this.options.dstCRS),
			_defCRS("EPSG:4326"),
		]).then(() => {
			const arruga = new LineArrugator(
				proj4(this.options.srcCRS, this.options.dstCRS).forward,
				latlngs
			);

			arruga.epsilon = this.options.epsilon;

			const arrugated = arruga.output();
			const mercatorify = proj4(this.options.dstCRS, "EPSG:4326").forward;
			const arrugatedLatLngs = arrugated
				.map(mercatorify)
				.map(L.GeoJSON.coordsToLatLng);

			this.setLatLngs(arrugatedLatLngs);

			this.fire("projected");
		});
	},
});
