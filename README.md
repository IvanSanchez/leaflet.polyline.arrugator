# Leaflet.Polyline.Arrugator

Displays reprojected polylines.

Leverages `proj4` (AKA `proj4js`), and a subset of [Arrugator](https://gitlab.com/IvanSanchez/arrugator); pulls CRS data from https://crs-explorer.proj.org and automagically projects the coordinates.

Hastily made during the [2024 Évora OGC/OSGeo/ASF codesprint](https://github.com/opengeospatial/developer-events/wiki/2024-Joint-OGC-%E2%80%93-OSGeo-%E2%80%93-ASF-Code-Sprint).


### Demo

https://ivansanchez.gitlab.io/Leaflet.Polyline.Arrugator/demo.html

See also https://ivansanchez.gitlab.io/Leaflet.ImageOverlay.Arrugator/demo.html for the `ImageOverlay` counterpart of that demo. L.Polyline.Arrugator is meant to
complement use cases where the bounding boxes of reprojected images are
interesting.

### Usage

As shown in the demo:

```
const line = new L.Polyline.Arrugator(
	[
		[-183622.3, 7996344.0],
		[-183622.3, 6396344.0],
		[1416377.7, 6396344.0],
		[1416377.7, 7996344.0],
		[-183622.3, 7996344.0],
	],
	{
		srcCRS: "EPSG:25833",
		epsilon: 10000, // Square of the max error, in units of the destination CRS (EPSG:3857)
		// dstCRS: "EPSG:3857" // Optional, assumed to be EPSG:3857
	}
).addTo(map);
```

Note that the input must be an array of `[x, y]` coordinates in the `srcCRS` - they are not `L.LatLng`s!

### Caveats

- Expects `proj4` to be defined as a global
- Assumes EPSG:4326 as the Leaflet data CRS for `LatLng`s. Will most profably fail when used together with `proj4leaflet`.
- No `package.json` just yet; that also means no NPM package just yet.

### Legalese

Under MIT license. See the `LICENSE` file for details.


